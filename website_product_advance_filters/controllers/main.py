# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website.controllers.main import QueryURL

import logging
_logger = logging.getLogger(__name__)

class WebsiteSale(WebsiteSale):

    def get_operated_list(self, set_list, operator):
        if set_list:
            a = set_list[0]
            for v in set_list[1:]:
                if operator == 'or':
                    a = a | v
                else:
                    a = a & v
            return a
        return []

    def _get_search_domain(self, search, category, attrib_values):
        product_ids = False
        res = super(WebsiteSale,self)._get_search_domain(search, category, attrib_values)
        filter_list = request.httprequest.args.getlist('filter')
        filter_values = [[int(x) for x in v.split("-")] for v in filter_list if v]
        if filter_values:
            fltr_operator = request.env['res.config.settings'].sudo().get_values().get('fltr_operator', 'and')

            # Filter Values Operator
            fltr_dict = {}
            for v in filter_values:
                filter_value_obj = request.env["product.filter.value"].browse(v[1])
                product_ids = filter_value_obj.product_ids.ids if filter_value_obj.product_ids else []
                if v[0] in fltr_dict.keys():
                    fltr_dict[v[0]].append(set(product_ids))
                else:
                    fltr_dict[v[0]] = [set(product_ids)]

            # Filters Operator
            product_ids = []
            for fltr, values in fltr_dict.items():
                fltr_obj = request.env["product.filter"].browse(fltr)
                operator = fltr_obj.value_operator if fltr_obj.value_operator else 'or'
                data = self.get_operated_list(values, operator)
                product_ids.append(data)

            product_ids = list(self.get_operated_list(product_ids, fltr_operator))
            res+=[('id','in',product_ids)]
        return res


    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        filter_ids = []
        ProductFilter = request.env['product.filter']
        filter_value_objs = False
        sel_fltr_name = {}

        filter_list = request.httprequest.args.getlist('filter')
        filter_values = [[int(x) for x in v.split("-")] for v in filter_list if v]
        filter_value_ids = {v[1] for v in filter_values}

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]

        if filter_value_ids:
            filter_value_objs = request.env["product.filter.value"].browse(filter_value_ids)
            for rec in filter_value_objs:
                data = sel_fltr_name.get(rec.filter_id.id,False)
                if data:
                    sel_fltr_name[rec.filter_id.id] = data+rec.name + ', '
                else:
                    sel_fltr_name[rec.filter_id.id] = rec.name + ', '

        keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list, filter=filter_list, order=post.get('order'))
        res = super(WebsiteSale, self).shop(page, category, search, ppg, **post)

        filter_ids = ProductFilter.search([('value_ids','!=',False),('website_published','=',True)])
        if category:
            category = request.env['product.public.category'].browse(int(category))
            if category.filter_ids:
                filter_ids = ProductFilter.search([('value_ids','!=',False),('filter_activation','=','all_cat'),('website_published','=',True)])
                category_fltrs = category.filter_ids.filtered('website_published')
                temp_fltr_ids = set(category_fltrs.ids + filter_ids.ids) if filter_ids else category_fltrs.ids
                filter_ids = ProductFilter.browse(temp_fltr_ids)

        res.qcontext.update({
            'filter_ids': filter_ids,
            'sel_fltr_name' : sel_fltr_name if sel_fltr_name else False,
            'selected_value_ids' : filter_value_objs,
            'keep': keep,
        })
        return res
