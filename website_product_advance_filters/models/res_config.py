# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################


from odoo import models, fields, api, _

import logging
_logger = logging.getLogger(__name__)


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    fltr_operator = fields.Selection(selection=[('or','OR'),('and','AND')], string="Filter Operator", default="or")

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.default'].sudo().set('res.config.settings', 'fltr_operator', self.fltr_operator)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        fltr_operator = self.env['ir.default'].get('res.config.settings', 'fltr_operator')
        res.update(
            fltr_operator = fltr_operator
        )
        return res
