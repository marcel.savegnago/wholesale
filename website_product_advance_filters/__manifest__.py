# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################
{
    "name"              :  "Website Product Advance Filters",
    "summary"           :  "This module allows a customer to narrow down a set of products to meet their needs and save time. Customer can extract specific items as per their needs.",
    "category"          :  "Website",
    "version"           :  "1.1.1",
    "sequence"          :  1,
    "author"            :  "Webkul Software Pvt. Ltd.",
    "license"           :  "Other proprietary",
    "website"           :  "https://store.webkul.com/Odoo-Website-Product-Advance-Filters.html",
    "description"       :  "",
    "live_test_url"     :  "http://odoodemo.webkul.com/?module=website_product_advance_filters",
    "depends"           :  [
                            'website_base_filter_attribute',
    ],
    "data"              :  [
                                'security/adv_filters_security_view.xml',
                                'security/ir.model.access.csv',
                                'data/res_config_data.xml',
                                'views/advance_filter_view.xml',
                                'views/inherit_product_view.xml',
                                'views/res_config_view.xml',
                                'views/template.xml',
    ],
    "demo"              :  [
    ],
    "images"            :  ['static/description/Banner.png'],
    "application"       :  True,
    "installable"       :  True,
    "auto_install"      :  False,
    "price"             :  69,
    "currency"          :  "EUR",
    "pre_init_hook"     :  "pre_init_check",
}
