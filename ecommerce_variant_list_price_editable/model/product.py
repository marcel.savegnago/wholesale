# -*- coding: utf-8 -*-
from odoo import fields, models, api
 
class Product(models.Model):
    _inherit = "product.product"
 
    lst_price = fields.Float(compute='_compute_lst_price', inverse='_inverse_product_lst_price',)
    list_price = fields.Float(compute='_compute_list_price')
    fix_price = fields.Float(string='Fix Price')
     
    @api.multi
    @api.depends('fix_price')
    def _compute_lst_price(self):
        to_uom = None
        if 'uom' in self._context:
            to_uom = self.env['uom.uom'].browse([self._context['uom']])
        for product in self:
            price = product.fix_price or product.list_price
            if to_uom:
                price = product.uom_id._compute_price(price, to_uom)
            product.lst_price = price
             
 
    @api.multi
    def _compute_list_price(self):
        for product in self:
            price = product.fix_price or product.product_tmpl_id.list_price
            to_uom = None
            if 'uom' in self._context:
                to_uom = self.env['uom.uom'].browse([self._context['uom']])
            if to_uom:
                price = product.uom_id._compute_price(price, to_uom)
            product.list_price = price
 
    @api.multi
    def _inverse_product_lst_price(self):
        for product in self:
            vals = {}
            to_uom = None
            if 'uom' in self._context:
                to_uom = self.env['uom.uom'].browse([self._context['uom']])
            if to_uom:
                vals['fix_price'] = product.uom_id._compute_price(product.lst_price, to_uom)
            else:
                vals['fix_price'] = product.lst_price
            if product.product_variant_count == 1:
                product.product_tmpl_id.list_price = vals['fix_price']
            product.write(vals)
        
class ProductTemplate(models.Model):
    _inherit = "product.template"
 
    def _update_fix_price(self, vals):
        if 'list_price' in vals:
            self.mapped('product_variant_ids').write({
                'fix_price': vals['list_price']})
 
    @api.model
    def create(self, vals):
        result = super(ProductTemplate, self).create(vals)
        result._update_fix_price(vals)
        return result
 
    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        for template in self:
            if len(template.product_variant_ids) < 2:
                template._update_fix_price(vals)
            else:
                for variant_id in template.product_variant_ids:
                    if not variant_id.fix_price:
                        if 'list_price' in vals:
                            variant_id.write({'fix_price': vals['list_price']})
                        else:
                            variant_id.write({'fix_price': template.list_price})
        return res