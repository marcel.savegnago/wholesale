# -*- coding: utf-8 -*-
{
    # Theme information
    'name' : 'Product Extra Price Extension',
    'category' : 'Website',
    'version' : '12.0',
    'summary': 'With this App, list price will be directly editable per each variant.',
    'license': 'OPL-1',

    # Dependencies
    'depends': [
        'website_sale'
    ],

    # Views
    'data': [
             'view/product.xml',
             'templates/template.xml',
    ],
    
    'images': ['static/description/Product Extra Price Extension cover.jpg'],
    
    # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'maintener': 'Emipro Technologies Pvt. Ltd.',
    
    # Technical
    'installable': True,
    'application': False,
    'live_test_url': 'https://www.emiprotechnologies.com/free-trial?app=ecommerce-variant-list-price-editable&version=12&edition=enterprise',
    'price': 49.00,
    'currency': 'EUR',
}
